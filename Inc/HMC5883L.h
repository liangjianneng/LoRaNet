#include <stdio.h>

extern void InitHMC5883L(void);
extern int16_t ReadDataHMC5883LX(void);
extern int16_t ReadDataHMC5883LY(void);
extern int16_t ReadDataHMC5883LZ(void);
extern int16_t XFilter(void);
extern int16_t YFilter(void);
extern int16_t ZFilter(void);

extern int16_t Xaxis,Yaxis,Zaxis;
extern int16_t Xinitial,Yinitial,Zinitial;
extern uint8_t XCheck_car(void);
extern uint8_t YCheck_car(void);
extern uint8_t ZCheck_car(void);
extern void Read_BaseValue(void);