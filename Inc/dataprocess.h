#ifndef __DATAPROCESS_H
#define __DATAPROCESS_H

#include "stm32f0xx.h"

#include "netprocess.h"

//extern uint8_t startUpTimeHours;
//extern uint8_t startUpTimeMinute;
//extern uint8_t startUpTimeSeconds;
//extern uint32_t startUpTimeSubSeconds;
// 
//extern uint8_t DataUpTimeHours;
//extern uint8_t DataUpTimeMinute;
//extern uint8_t DataUpTimeSeconds;
//extern uint32_t DataUpTimeSubSeconds;

extern uint8_t startUpDateHours;
extern uint8_t startUpDateMinute;
extern uint8_t startUpDateSeconds;
extern uint16_t startUpDateSubSeconds;

extern void SendJionNetPacke(void);
extern uint8_t Sx127xDataGet(void);
uint8_t SlaveProtocolAnalysis(uint8_t *buff,uint8_t len);
void NetDataProtocolAnalysis(uint8_t *buff,uint8_t len);
uint8_t JionNetProtocolAnalysis(uint8_t *buff,uint8_t len);
extern SlaveInfo slaveNativeInfo_t;
void UartDmaGet(void);
extern void SendSensorDataUP(void);
extern uint8_t MasterProtocolAnalysis(uint8_t *buff,uint8_t len);  

extern SlaveInfo slaveNetInfo_t[NodeNumber];

#endif /* __DATAPROCESS_H */