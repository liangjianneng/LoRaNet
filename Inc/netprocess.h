#ifndef __NETPROCESS_H
#define __NETPROCESS_H

#include "stm32f0xx.h"

extern uint32_t * ADC_DMA_Value;

typedef enum
{
  JION_HEADER = 0,
  JION_LENGHT,
  JION_TYPE,
  JION_PANID,
  JION_ADDR,
  JION_CRC
}JionProtocol;

typedef struct
{
  uint8_t msgHead;
  uint8_t dataLength;
  uint8_t netType;
  uint8_t netPanid[2];
  uint8_t deviceAddr[2];
  uint8_t crcCheck;
}SlaveJionNet;

typedef struct
{
  uint8_t netmsgHead;
  uint8_t netPanid[2];
  uint8_t msgHead;
  uint8_t dataLength;
  uint8_t dataType;
  uint8_t deviceAddr[2];
  uint8_t sensorType;
  uint8_t buff[4];
  uint8_t crcCheck;
}SlaveDataNet;



typedef enum
{
  NO_JION,
  JIONING,
  JIONTIMEOUT,
  AGAIN_JION_NET,
  JIONDONE
}DeviceJionStatus;

typedef struct
{
  uint8_t deviceType;
  DeviceJionStatus deviceNetStatus;
  uint8_t deviceAddr[2];
  uint8_t deviceId;
}SlaveInfo;



typedef enum
{
  No_Node_Jion_Flag = 0,
  Node_Jion_Finish_Flag,
  Node_Jion_No_Finish_Flag,
  New_Node_Jion_Flag
}DeviceJionFlag;


//,deviceNetStatus

typedef struct
{
  uint8_t msgHead;
  uint8_t dataLength;
  uint8_t netType;
  uint8_t netPanid[2];
  uint8_t timeData[5];
  uint8_t crcCheck;
}SlaveRtcSync;

extern uint8_t startUpTimeHours;
extern uint8_t startUpTimeMinute;
extern uint8_t startUpTimeSeconds;
extern uint32_t startUpTimeSubSeconds;
 
extern uint8_t DataUpTimeHours;
extern uint8_t DataUpTimeMinute;
extern uint8_t DataUpTimeSeconds;
extern uint32_t DataUpTimeSubSeconds;

extern volatile  uint16_t oldNodeNumber;

#define ADC_NUM 5
#define NodeNumber 20

//extern uint32_t * ADC_DMA_Value;
extern volatile uint16_t currentDeviceNumber;
extern volatile uint32_t DataUpTime;
extern DeviceJionFlag WaitJionNetFinish(uint8_t timout);
extern uint8_t SlaveJionNetFuction(void);
extern void MasterSendClockData(void);
extern void SlaveGetSendTime(void);
uint16_t RandomNumber(void);

//typedef enum
//{
//  
//}SlaveInfo;

#endif /* __NETPROCESS_H */

