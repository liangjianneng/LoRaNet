/**
  ******************************************************************************
  * File Name          : RTC.c
  * Description        : This file provides code for the configuration
  *                      of the RTC instances.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "rtc.h"

/* USER CODE BEGIN 0 */

#include <string.h>
#include "dataprocess.h"
#include "netprocess.h"

//uint8_t startUpDateHours = 0;
//uint8_t startUpDateMinute = 0;
//uint8_t startUpDateSeconds = 0;
//uint16_t startUpDateSubSeconds = 0;   
//
//uint8_t DataUpTimeHours = 0;
//uint8_t DataUpTimeMinute = 0;
//uint8_t DataUpTimeSeconds = 0;
//uint32_t DataUpTimeSubSeconds = 0;

#define CLOCKHOURS 5

//#define  gAlarm  sAlarm

RTC_AlarmTypeDef gAlarm;

volatile uint8_t sendUpDataFlag = 0;
  
volatile uint8_t SendClockFlag = 0;

/* USER CODE END 0 */

RTC_HandleTypeDef hrtc;

/* RTC init function */
void MX_RTC_Init(void)
{
  RTC_TimeTypeDef sTime = {0};
  RTC_DateTypeDef sDate = {0};
  RTC_AlarmTypeDef sAlarm = {0};

  /** Initialize RTC Only 
  */
  hrtc.Instance = RTC;
  hrtc.Init.HourFormat = RTC_HOURFORMAT_24;
  hrtc.Init.AsynchPrediv = 125-1;
  hrtc.Init.SynchPrediv = 2000-1;
  hrtc.Init.OutPut = RTC_OUTPUT_DISABLE;
  hrtc.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
  hrtc.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;
  if (HAL_RTC_Init(&hrtc) != HAL_OK)
  {
    Error_Handler();
  }

  /* USER CODE BEGIN Check_RTC_BKUP */
    
  /* USER CODE END Check_RTC_BKUP */

  /** Initialize RTC and set the Time and Date 
  */
  sTime.Hours = 0x0;
  sTime.Minutes = 0x0;
  sTime.Seconds = 0x0;
  sTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
  sTime.StoreOperation = RTC_STOREOPERATION_RESET;
  if (HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BCD) != HAL_OK)
  {
    Error_Handler();
  }
  sDate.WeekDay = RTC_WEEKDAY_MONDAY;
  sDate.Month = RTC_MONTH_JANUARY;
  sDate.Date = 0x1;
  sDate.Year = 0x18;

  if (HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BCD) != HAL_OK)
  {
    Error_Handler();
  }
  /** Enable the Alarm A 
  */
  sAlarm.AlarmTime.Hours = 0x0;
  sAlarm.AlarmTime.Minutes = 0x0;
  sAlarm.AlarmTime.Seconds = 0x0;
  sAlarm.AlarmTime.SubSeconds = 0x0;
  sAlarm.AlarmTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
  sAlarm.AlarmTime.StoreOperation = RTC_STOREOPERATION_RESET;
  sAlarm.AlarmMask = RTC_ALARMMASK_NONE;
  sAlarm.AlarmSubSecondMask = RTC_ALARMSUBSECONDMASK_ALL;
  sAlarm.AlarmDateWeekDaySel = RTC_ALARMDATEWEEKDAYSEL_DATE;
  sAlarm.AlarmDateWeekDay = 0x1;
  sAlarm.Alarm = RTC_ALARM_A;
  if (HAL_RTC_SetAlarm_IT(&hrtc, &sAlarm, RTC_FORMAT_BCD) != HAL_OK)
  {
    Error_Handler();
  }

}

void HAL_RTC_MspInit(RTC_HandleTypeDef* rtcHandle)
{

  if(rtcHandle->Instance==RTC)
  {
  /* USER CODE BEGIN RTC_MspInit 0 */

  /* USER CODE END RTC_MspInit 0 */
    /* RTC clock enable */
    __HAL_RCC_RTC_ENABLE();

    /* RTC interrupt Init */
    HAL_NVIC_SetPriority(RTC_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(RTC_IRQn);
  /* USER CODE BEGIN RTC_MspInit 1 */

  /* USER CODE END RTC_MspInit 1 */
  }
}

void HAL_RTC_MspDeInit(RTC_HandleTypeDef* rtcHandle)
{

  if(rtcHandle->Instance==RTC)
  {
  /* USER CODE BEGIN RTC_MspDeInit 0 */

  /* USER CODE END RTC_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_RTC_DISABLE();

    /* RTC interrupt Deinit */
    HAL_NVIC_DisableIRQ(RTC_IRQn);
  /* USER CODE BEGIN RTC_MspDeInit 1 */

  /* USER CODE END RTC_MspDeInit 1 */
  }
} 

/* USER CODE BEGIN 1 */

//**********************************//
//
//函数名称：HAL_RTC_AlarmAEventCallback   
//
//函数描述： 闹钟事件回调函数  
//
//函数参数：   RTC_HandleTypeDef *hrtc
//
//返回值：    无 
//
//创建者：    
//*******************************//
 
void HAL_RTC_AlarmAEventCallback(RTC_HandleTypeDef *hrtc)
{
  
  RTC_TimeTypeDef masterTime;
  RTC_TimeTypeDef SlaveTime;
  RTC_DateTypeDef masterDate;
  
#if MASTER  
  //置位同步时钟标志
    SendClockFlag = 0;
  //获取下次闹钟时间
    HAL_RTC_GetTime(hrtc, &masterTime, RTC_FORMAT_BIN);
    HAL_RTC_GetDate(hrtc, &masterDate, RTC_FORMAT_BIN);
    gAlarm.AlarmTime.Hours = masterTime.Hours + CLOCKHOURS;
    gAlarm.AlarmTime.Minutes = masterTime.Minutes;
    gAlarm.AlarmTime.Seconds = masterTime.Seconds;
    gAlarm.AlarmTime.SubSeconds = masterTime.SubSeconds;
    
#else
    sendUpDataFlag = 1;
    HAL_RTC_GetTime(hrtc, &SlaveTime, RTC_FORMAT_BIN);
    HAL_RTC_GetDate(hrtc, &masterDate, RTC_FORMAT_BIN);
    gAlarm.AlarmTime.Hours = SlaveTime.Hours + DataUpTimeHours;
    gAlarm.AlarmTime.Minutes = SlaveTime.Minutes + DataUpTimeMinute;
    gAlarm.AlarmTime.Seconds = SlaveTime.Seconds + DataUpTimeSeconds;
    gAlarm.AlarmTime.SubSeconds = SlaveTime.SubSeconds + DataUpTimeSubSeconds;
#endif
    
    
    if (gAlarm.AlarmTime.Seconds > 59)
    {
	   gAlarm.AlarmTime.Seconds -= 60;
	   gAlarm.AlarmTime.Minutes += 1;
    }
 
   if ( gAlarm.AlarmTime.Minutes >59)
   {
	   gAlarm.AlarmTime.Minutes -= 60;
	   gAlarm.AlarmTime.Hours += 1;
   }
   if (gAlarm.AlarmTime.Hours > 23)
   {
	   gAlarm.AlarmTime.Hours -= 24;
   }
    
   printf("RTC\n");
  //使能闹钟中断
    if (HAL_RTC_SetAlarm_IT(hrtc, &gAlarm, RTC_FORMAT_BIN) != HAL_OK)
    {
      //_Error_Handler(__FILE__, __LINE__);
      Error_Handler();
      printf("HAL_RTC_SetAlarm_IT error\n");
    }
}

//**********************************//
//
//函数名称：   GetTimeHMS
//
//函数描述：   时分秒转换
//
//函数参数：   uint32_t timeData,uint8_t *hours,uint8_t *minute,uint8_t *seconds,uint32_t *subSeconds
//
//返回值：     无
//
//创建者：     
//*******************************//
 
void GetTimeHMS(uint32_t timeData,uint8_t *hours,uint8_t *minute,uint8_t *seconds,uint32_t *subSeconds) 
{
	/* 获得亚秒 */
	*subSeconds = timeData % 1000;
	/* 获得秒钟*/
	timeData = timeData / 1000;
	*seconds = timeData % 60;
	/* 获得分钟*/
	timeData = timeData / 60;
	*minute = timeData % 60;
	/* 获得小时 */
	*hours = timeData / 60;
}



/* USER CODE END 1 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
