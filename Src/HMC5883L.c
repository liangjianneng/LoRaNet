#include <stdio.h>
#include <string.h>
#include "stm32f0xx.h"
#include "stdbool.h"
#include "HMC5883L.h"
#include "C:\Users\1\OneDrive\桌面\new - 自组网 - 改1\new - 自组网 - 改\new - 自组网 - 改\Inc\i2c.h"

#define ADDRESS_Write 0x3C      //写操作地址
#define ADDRESS_Read 0x3D       //读操作地址
#define MR 0x02                 //模式寄存器地址

#define XaxisMSB 0x03
#define XaxisLSB 0x04
#define YaxisMSB 0x05
#define YaxisLSB 0x06
#define ZaxisMSB 0x07
#define ZaxisLSB 0x08

#define FILTER_N 5

#define BUFNUM 2

int16_t Xaxis=0,Yaxis=0,Zaxis=0;

int16_t Xinitial, Yinitial, Zinitial;

int16_t filter_bufx[FILTER_N + 1];
int16_t filter_bufy[FILTER_N + 1];
int16_t filter_bufz[FILTER_N + 1];

int16_t check_bufx[BUFNUM + 1];
int16_t check_bufy[BUFNUM + 1];
int16_t check_bufz[BUFNUM + 1];

//初始化HMC5883L
void InitHMC5883L(void)
{
  uint8_t WriteCmd = 0x00;         //定义
  
  HAL_I2C_Mem_Write(&hi2c1, ADDRESS_Write, MR, 1, &WriteCmd, 1, 0x10);          //配置连续测量模式
}

//读取数据寄存器的值
int16_t ReadDataHMC5883LX(void)
{
  uint8_t ReadBuffer1[2] = {0};
  HAL_I2C_Mem_Read(&hi2c1, ADDRESS_Read, XaxisMSB, 1, ReadBuffer1, 1, 0x10);         //读取数据寄存器高位
  HAL_I2C_Mem_Read(&hi2c1, ADDRESS_Read, XaxisLSB, 1, ReadBuffer1 + 1, 1, 0x10);     //读取数据寄存器低位
  Xaxis = ((ReadBuffer1[0]*256) + ReadBuffer1[1]);                                   //计算磁场数据
  if(Xaxis>45000){
    Xaxis = -(65536-Xaxis);
  }     
  return Xaxis;
}

int16_t ReadDataHMC5883LY(void)
{
  uint8_t ReadBuffer1[2] = {0};
  HAL_I2C_Mem_Read(&hi2c1, ADDRESS_Read, YaxisMSB, 1, ReadBuffer1, 1, 0x10);
  HAL_I2C_Mem_Read(&hi2c1, ADDRESS_Read, YaxisLSB, 1, ReadBuffer1 + 1, 1, 0x10);
  Yaxis = ((ReadBuffer1[0]*256) + ReadBuffer1[1]);
  if(Yaxis>45000){
    Yaxis = -(65536-Yaxis);
  } 
  return Yaxis;
}  

int16_t ReadDataHMC5883LZ(void)
{
  uint8_t ReadBuffer1[2] = {0};
  HAL_I2C_Mem_Read(&hi2c1, ADDRESS_Read, ZaxisMSB, 1, ReadBuffer1, 1, 0x10);
  HAL_I2C_Mem_Read(&hi2c1, ADDRESS_Read, ZaxisLSB, 1, ReadBuffer1 + 1, 1, 0x10);
  Zaxis = ((ReadBuffer1[0]*256) + ReadBuffer1[1]);
  if(Zaxis>45000){
    Zaxis = -(65536-Zaxis);
  }    
  return Zaxis;
}

//读取基准值
void Read_BaseValue(void)
{
  Xinitial = ReadDataHMC5883LX();
  Yinitial = ReadDataHMC5883LY();
  Zinitial = ReadDataHMC5883LZ();
  printf("Xinitial = %d,Yinitial = %d,Zinitial = %d\n",Xinitial,Yinitial,Zinitial);
}

//滤波算法
int16_t XFilter(void) 
{
    int i;
    int16_t filter_sum = 0;
    for(i = 0; i < FILTER_N; i++) 
    {
        filter_bufx[i] = filter_bufx[i + 1];         // 所有数据左移，低位仍掉
        filter_sum += filter_bufx[i];
    }
    filter_bufx[FILTER_N] = ReadDataHMC5883LX();     //AD转换的值赋给数组最后一个值
    return (int)(filter_sum / FILTER_N);
}


int16_t YFilter(void) 
{
    int i;
    int16_t filter_sum = 0;
    for(i = 0; i < FILTER_N; i++) 
    {
        filter_bufy[i] = filter_bufy[i + 1];         // 所有数据左移，低位仍掉
        filter_sum += filter_bufy[i];
    }
    filter_bufy[FILTER_N] = ReadDataHMC5883LY();      //AD转换的值赋给数组最后一个值
    return (int)(filter_sum / FILTER_N);
}

int16_t ZFilter(void) 
{
    int i;
    int16_t filter_sum = 0;
    for(i = 0; i < FILTER_N; i++) 
    {
        filter_bufz[i] = filter_bufz[i + 1];         // 所有数据左移，低位仍掉
        filter_sum += filter_bufz[i];
    }
    filter_bufz[FILTER_N] = ReadDataHMC5883LZ();     //AD转换的值赋给数组最后一个值
    return (int)(filter_sum / FILTER_N);
}


//检测有无车辆
uint8_t XCheck_car(void)
{
	int i,a,b;
	int16_t sub[3] = {0};

	for(i = 0; i < BUFNUM; i++)
	{
           check_bufx[i] = check_bufx[i + 1];           //数据往左移
	}
        check_bufx[BUFNUM] = XFilter();                 //新的数据赋给最右的buff
                
        for(a = 0; a<=2; a++)
        {
          sub[a] = Xinitial - check_bufz[a];            //算出buff中的数据与初始值的差
          if(sub[a] <= 0)
          {
            sub[a] = -sub[a];
          }
        }
        
        for(b = 0; b<=2; b++)
        {
          if(sub[b] <= 150 || sub[b] >= 250){
            return 0;                                   //如果三个差值数据中有有一个小于150，返回零
          }
        }
        return 1;                                       //三个差值都大于150，返回1
}

uint8_t YCheck_car(void)
{
	int i,a,b;
	int16_t sub[3] = {0};

	for(i = 0; i < BUFNUM; i++)
	{
           check_bufy[i] = check_bufy[i + 1];           //数据往左移
	}
        check_bufy[BUFNUM] = YFilter();                 //新的数据赋给最右的buff
                
        for(a = 0; a<=2; a++)
        {
          sub[a] = Xinitial - check_bufz[a];            //算出buff中的数据与初始值的差
          if(sub[a] <= 0)
          {
            sub[a] = -sub[a];
          }
        }
        
        for(b = 0; b<=2; b++)
        {
          if(sub[b] <= 50 || sub[b] >= 150){
            return 0;                                   //如果三个差值数据中有有一个小于150，返回零
          }
        }
        return 1;                                       //三个差值都大于150，返回1
}


uint8_t ZCheck_car(void)
{
	int i,a,b;
	int16_t sub[3] = {0};
        printf("Xinitial = %d,Yinitial = %d,Zinitial = %d\n",Xinitial,Yinitial,Zinitial);
	for(i = 0; i < BUFNUM; i++)
	{
           check_bufz[i] = check_bufz[i + 1];           //数据往左移
	}
        check_bufz[BUFNUM] = ZFilter();                 //新的数据赋给最右的buff
                
        for(a = 0; a<=2; a++)
        {
          sub[a] = Xinitial - check_bufz[a];            //算出buff中的数据与初始值的差
          if(sub[a] <= 0)
          {
            sub[a] = -sub[a];
          }
        }
        
        for(b = 0; b<=2; b++)
        {
          if(sub[b] <= 250 || sub[b] >= 350){
            return 0;                                   //如果三个差值数据中有有一个小于150，返回零
          }
        }
        return 1;                                       //三个差值都大于150，返回1
}

