/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "dma.h"
#include "i2c.h"
#include "rtc.h"
#include "spi.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

#include "adc.h"
#include "dma.h"
#include "rtc.h"
#include "spi.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"
#include <stdio.h>
#include <string.h>

#include "led.h"
    
#include "protocol.h"
#include "netprocess.h"
#include "dataprocess.h"

//sx1278
#include "platform.h"
#include "sx1276-Hal.h"
#include "radio.h"
#include "sx1276-LoRa.h"
#include "sx1276-LoRaMisc.h"
#include "netprocess.h"
#include "dataprocess.h"
#include "HMC5883L.h"
#include "stm32f0xx.h"
    
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

#define BUFFERSIZE 4
//uint8_t PingMsg[] = "PING";
//uint8_t PongMsg[] = "PONG"; 
uint16_t BufferSize = BUFFERSIZE;
uint8_t Buffer[BUFFERSIZE];


#ifdef MASTER
extern uint8_t EnableMaster = true;
#else
extern uint8_t EnableMaster = false;
#endif

uint32_t Master_TxNumber = 0;
uint32_t Master_RxNumber = 0;

uint32_t Slave_TxNumber = 0;
uint32_t Slave_RxNumber = 0;

tRadioDriver *Radio = NULL;


extern volatile uint8_t SendClockFlag;
DeviceJionFlag	JionDeviceStatu = No_Node_Jion_Flag;
volatile uint8_t MasterSendTimeSliceFlag = 0;
volatile uint8_t SendDataOkFlag = 0;

USART_RECEIVETYPE UsartType1;
uint8_t a_Usart1_RxBuffer[RXLENGHT];

int16_t X1,Y1,Z1;
uint8_t Xcheck,Ycheck,Zcheck;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

void LedBlink( tLed led );



void MLCD_Show(void)
{
  
}

void SLCD_Show(void)
{
  
}



/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
  //int16_t X1,Y1,Z1;
  uint8_t RegVersion = 0;

  /* USER CODE END 1 */
  

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */
  uint32_t DelayTime = 0;

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_ADC_Init();
  //MX_RTC_Init();
  MX_SPI1_Init();
  MX_TIM2_Init();
  MX_USART1_UART_Init();
  MX_I2C1_Init();
  /* USER CODE BEGIN 2 */
  HAL_SPI_DeInit(&hspi1);
  MX_SPI1_Init();
  
  __HAL_UART_ENABLE_IT(&huart1,UART_IT_IDLE);
  HAL_UART_Receive_DMA(&huart1,a_Usart1_RxBuffer,RXLENGHT);
  
  SX1276Read( REG_LR_VERSION, &RegVersion );
  if(RegVersion != 0x12)
  {
    printf("LoRa read Error!\r\n");
    printf("LoRa RegVersion = %d!\r\n",RegVersion);
  }
  else
  {
    printf("LoRa read Ok!\r\n");
    printf("LoRa RegVersion = %d!\r\n",RegVersion); 
  }

  
  LedOff(LED_RX);
  LedOff(LED_TX);
  LedOff(LED_NT);
  
  Radio = RadioDriverInit();
  printf("system init is ok!\n");
  Radio->Init();
  Radio->StartRx();
  
//#ifdef MASTER
//  Radio->SetTxPacket(PingMsg,strlen((const char*)PingMsg));
//#else
//  Radio->StartRx();
//#endif
  
#if SLAVE  
  //获取随机入网时间  
  DelayTime = RandomNumber();  
  printf("JionTime = %d\n",DelayTime);  
  HAL_Delay(DelayTime);  
  //等待入网成功  
  while (SlaveJionNetFuction());  
  //获取节点发送时间片  
  SlaveGetSendTime();  
#else    
  //主机直接初始化RTC    
  MX_RTC_Init();
  HAL_TIM_Base_Start_IT(&htim2);
#endif
  
  InitHMC5883L();
  Read_BaseValue();
  while(1)
  {
//    //ReadDataHMC5883L();
//    X1 = XFilter();
//    Y1 = YFilter();
//    Z1 = ZFilter();
//    printf("Xaxis = %d, Yaxis = %d, Zaxis = %d\n", Xaxis, Yaxis, Zaxis);
//    printf("X1 = %d, Y1 = %d, Z1 = %d\n", X1, Y1, Z1);
//    HAL_Delay(300);
//    Xcheck = XCheck_car();
//    Ycheck = YCheck_car();
//    Zcheck = ZCheck_car();
//    X1 = XFilter();
//    Y1 = YFilter();
//    Z1 = ZFilter();
//    printf("X1 = %d, Y1 = %d, Z1 = %d\n", X1, Y1, Z1);
//    if(Xcheck==1 && Ycheck==1 && Zcheck==1){
//      printf("有车状态\n");
//    }else{
//      printf("无车状态\n");
//    }
//    HAL_Delay(400);
  }
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
      //HAL_Delay(200);
    /* USER CODE END WHILE */
    
    Sx127xDataGet();
        //printf("Sx127xDataGet1 end,sendUpDataFlag=%d\n",sendUpDataFlag);   
    
#if SLAVE
    if(sendUpDataFlag == 1)
    {
      SendSensorDataUP();
      //printf("SendSensorDataUP end,sendUpDataFlag=%d\n",sendUpDataFlag);   
      sendUpDataFlag = 0;
    }
//        SendSensorDataUP();
//        HAL_Delay(1000);
    
    Xcheck = XCheck_car();
    Ycheck = YCheck_car();
    Zcheck = ZCheck_car();
    X1 = XFilter();
    Y1 = YFilter();
    Z1 = ZFilter();
    printf("X1 = %d, Y1 = %d, Z1 = %d\n", X1, Y1, Z1);
    if(Xcheck==1 && Ycheck==1 && Zcheck==1){
      printf("有车状态\n");
    }else{
      printf("无车状态\n");
    }
    HAL_Delay(400);

#else
    UartDmaGet();
    //printf("UartDmaGet end\n");

    //等待节点入网
    if (JionDeviceStatu != Node_Jion_Finish_Flag)
    {
      printf("main 等待加入网络\n");
      JionDeviceStatu = WaitJionNetFinish(10);
      //printf("WaitJionNetFinish end\n");
    }
     
        /* 有新节点加入 */
    if (currentDeviceNumber != oldNodeNumber)   
    {
      printf("main 新节点加入网络\n");
      HAL_TIM_Base_Start_IT(&htim2);
      JionDeviceStatu = New_Node_Jion_Flag;   
      SendClockFlag = 0; //发送分时时间片
      //printf("HAL_TIM_Base_Start_IT1 end\n");
    }
       /* 有旧节点加入 */
    for (int i = 0; i < currentDeviceNumber;i++)
    {
      /* 查询是否有旧节点重新加入*/
      if (slaveNetInfo_t[i].deviceNetStatus == AGAIN_JION_NET)
      {
        printf("main 旧节点加入网络\n");
        slaveNetInfo_t[i].deviceNetStatus = JIONDONE;
        JionDeviceStatu = New_Node_Jion_Flag;
		SendClockFlag = 0; //发送分时时间片
        HAL_TIM_Base_Start_IT(&htim2);
        //printf("HAL_TIM_Base_Start_IT2 end\n");
      }
    }
    
        /* 给从机分发时间片 */
    if ((JionDeviceStatu == Node_Jion_Finish_Flag)&&(SendClockFlag == 0)
        &&(currentDeviceNumber != 0))
    {
		if (SendDataOkFlag == 1) {
			SendDataOkFlag = 0;
                        printf("main 发送时钟同步\n");
			//告诉所有节点开始上传数据
			MasterSendClockData();
                        //printf("MasterSendClockData end\n");
			SendClockFlag = 1;
			while(!SendDataOkFlag)  //等待发送完成
			{
				Sx127xDataGet();
                                //printf("while Sx127xDataGet\n");
			}
			SendDataOkFlag = 1;
		}
    }
#endif
    
    

    /* USER CODE BEGIN 3 */
    
//    if(EnableMaster == true)
//    {
//      MLCD_Show();
//    //  Master_Task();
//    }
//    else
//    {
//      SLCD_Show();
//    //  Slave_Task();
//    }
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_HSI14
                              |RCC_OSCILLATORTYPE_LSI|RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSI14State = RCC_HSI14_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.HSI14CalibrationValue = 16;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL6;
  RCC_OscInitStruct.PLL.PREDIV = RCC_PREDIV_DIV1;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1|RCC_PERIPHCLK_I2C1
                              |RCC_PERIPHCLK_RTC;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK1;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_HSI;
  PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSI;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

int fputc(int ch,FILE *f)
{
  while((USART1->ISR&0x40) == 0);
  USART1->TDR = (uint8_t)ch;
  return ch;
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(char *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
